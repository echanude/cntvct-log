# cntvct logger

A trivial binary to dump the content of cntvct from CLI in human readable
units. Sub-packages provide a Systemd service to hook to Systemd targets and a
dracut module lets one do just that from the initramfs.

This comes with all the caveat associated with _cntvct_.

## Example

```
$ rpkg local
$ sudo install <path to RPM>{,-systemd,-dracut}*.rpm
$ sudo dracut --add cntvct --show-modules <path-to-initramfs> # This will register logs at local-fs, basic, initrd and initrd-switch-root targets. The dracut module can be modified to change the behavior.
$ sudo systemctl enable cntvct@basic.service # enable the log when basic.target is reached.
$ journalctl -b0 -o short-monotonic | grep 'cntvct@.*[0-9.]s\.'
[    0.582683] localhost cntvct@local-fs[194]: 5.409352s.
[    0.885672] localhost cntvct@basic[228]: 5.768381s.
[    2.022430] localhost cntvct@initrd[276]: 6.905183s.
[   20.222714] localhost cntvct@initrd-switch-root[345]: 25.105452s.
```
